# SPM

Solar Panel Monitoring System 

**PHP FILES WILL BE UPLOADED AT A LATER DATE**

Collects data from Sunnyboy inverter and uploads to a MySQL database
Runs on a Raspberry Pi with DHT22, BMP180 and MAX3232.

# Database Configuration
1. Run spm.sql
2. Create user with SELECT, INSERT access for SPM database

# Pi Configuration
1. Connect to wifi
2. Copy spm.py, variables.py to a folder on the desktop
3. Modify variables.py to contain relevant details for your SQL server
4. Run crontab -e
5. At the end of the file, insert:
 "* * * * * sudo python /home/pi/Desktop/spm/spm.py > /home/pi/Desktop/spm/log.txt" without quotes to run every minute.
6. Save crontab and restart the pi