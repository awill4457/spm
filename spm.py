#THIS RUNS VIA CRONJOB: TO CONFIGURE DO
#crontab -e
#ENTER THE FOLLOWING AT THE END
#"* * * * * sudo python /home/pi/Desktop/spm/spm.py > /home/pi/Desktop/spm/log.txt"

import serial
import time
import Adafruit_DHT # Import Temperature Sensor
import MySQLdb
from datetime import datetime
import math
import Adafruit_BMP.BMP085 as BMP085 # Import Pressure Sensor
import subprocess
import struct
from variables import *

#Serial Commands to SEND
#Tell the inverter to collect all the data
c10 = "\xaa\xaa\x68\x04\x04\x68\x00\x00\x00\x00\x80\x00\x0a\x86\xe4\xcc\x3f\xff\x02\x16\x55\x55"
print (c10)
#Send me all your data!
c11 = "\xAA\xAA\x68\x03\x03\x68\x00\x00\x03\x00\x00\x00\x0b\x0f\x09\x00\x26\x00\x16\x55\x55"
print (c11)
#Initialise the serial port
ser = serial.Serial(port='/dev/serial0', baudrate=1200, timeout=5)
#Try connect to the pressure sensor
try:
    BMP = BMP085.BMP085()
except IOError:
    subprocess.call(['i2cdetect', '-y', '1'])
    BMP = BMP085.BMP085()

# Send a command to the Inverter
def sendPoll(command):
    ser.write(command)

#Read & process the data received from the Inverter, & send the results onto the PHP Server
def calcStats(strlength):
    print("Reading Serial")
    print("Bytes to read: "+str(ser.inWaiting()))
    rx1=[0]
    count=0
    if(ser.inWaiting()>0):
        while (int(ser.read(size=1).encode("hex"),16)!=170):
            print("Thats not 170!")
        rx1.append(170)
        while True:
            inp = ser.read(size=1) #read a byte
            rx1.append(int(inp.encode("hex"),16))
            if ser.inWaiting()==0: #if the incoming byte is 0x16
                break
            count=count+1
        rx1.pop(0)

    print(type(rx1))
    print("Read serial")
    print(rx1)

    #Read the temperature sensor
    hmty, temp = Adafruit_DHT.read_retry(22,4)
    #Round the values to 3 significant figures
    hmty=str(math.ceil(hmty*100)/100)
    temp=str(math.ceil(temp*100)/100)
    print("Temp: "+temp)
    print("Hmty: "+hmty)
    try:
        pressure='{0:0.2f}'.format(BMP.read_pressure()) # The local pressure
    except IOError:
        subprocess.call(['i2cdetect', '-y', '1'])
        pressure='{0:0.2f}'.format(BMP.read_pressure()) # The local pressure
    print("Pressure: "+pressure)
    user_id="15"
    insert = "INSERT INTO `results` (user_id, date, time, Upv, Pac, eTotal, hTotal, hmty, temp, pres) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    #Get time and date for SQL command
    date= datetime.now().strftime('%Y-%m-%d')
    time= datetime.now().strftime('%H:%M:%S')
#    if(1==2):
    if(len(rx1)!=1):
        #Read from the Inverter
        rx27=rx1[27]
        rx26=rx1[26]
        Upv=rx27*256+rx26
        StrUpv = str(Upv)
        print("UPV: "+StrUpv)

        rx39=(rx1[39])
        rx38=(rx1[38])
        Pac=rx39*256+rx38
        StrPac = str(Pac)
        print("PAC: "+StrPac)
      
        rx69 = rx1[69]*(16777216)
        rx68 = rx1[68]*(65536)
        rx67 = rx1[67]*(256)
        rx66 = rx1[66]
        rx65 = rx1[65]*(16777216)
        rx64 = rx1[64]*(65536)
        rx63 = rx1[63]*(256)
        rx62 = rx1[62]

        eTotal=rx65+rx64+rx63+rx62;
        StreTotal= str(eTotal);
        print("eTotal: "+StreTotal)

        hTotal=rx69+rx68+rx67+rx66;
        hTotal=hTotal/3600;
        StrhTotal= str(hTotal);
        print("hTotal: "+StrhTotal)
    rx1=0

    try:
        Args=(user_id, date, time, Upv, Pac, eTotal, hTotal, hmty, temp, pressure)
        print(insert,Args)
    except NameError:
        Args= (user_id, date, time, None, None, None, None, hmty, temp, pressure)
        print(insert,Args)
    try:
        db=MySQLdb.connect(host=dburl(), user=dbuser(), passwd=dbpassword(), db=dbname())
        cur=db.cursor()
        cur.execute(insert,Args)
        db.commit()
        db.close()
    except MySQLdb.OperationalError as e:
        print("Failed to insert, Database offline")
        

#The main loop
def main():
    #Tell the inverter to take a snapshot of the data
    #Broadcast command - no response
    print("Sending C10")
    sendPoll(c10)
    time.sleep(1)

    print("Sending C11")
    #Request the data in the previous snapshot
    sendPoll(c11)
    time.sleep(1)
    #Process the raw data & send onto the PHP Server
    calcStats(91)

main()
